package com.edgewoodsoftware.covid.riteaid.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CovidStoresResponseDTO {
    @JsonProperty("Data")
    private DataDTO data;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("ErrCde")
    private Object errorCode;

    @JsonProperty("ErrMsg")
    private Object errorMessage;

    @JsonProperty("ErrMsgDtl")
    private Object errorMessageDetail;
}


