package com.edgewoodsoftware.covid.riteaid.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreSlotsResponseDTO {
    @JsonProperty("Data")
    private StoreSlotsDataDTO data;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("ErrCde")
    private String errorCode;

    @JsonProperty("ErrMsg")
    private String errorMessage;

    @JsonProperty("ErrMsgDtl")
    private String errorMessageDetail;
}

