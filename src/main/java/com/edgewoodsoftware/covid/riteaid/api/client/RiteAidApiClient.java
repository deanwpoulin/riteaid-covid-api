package com.edgewoodsoftware.covid.riteaid.api.client;

import com.edgewoodsoftware.covid.riteaid.api.dto.CovidStoresResponseDTO;
import com.edgewoodsoftware.covid.riteaid.api.dto.StoreSlotsResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "riteaid-client", url = "${riteaid-api.url}")
public interface RiteAidApiClient {
    // https://www.riteaid.com/services/ext/v2/vaccine/checkSlots?storeNumber=7772
    @GetMapping("/services/ext/v2/vaccine/checkSlots")
    StoreSlotsResponseDTO checkSlots(@RequestParam int storeNumber);

    // https://www.riteaid.com/services/ext/v2/stores/getStores?address=19460&radius=20&pharmacyOnly=true&globalZipCodeRequired=true
    @GetMapping("/services/ext/v2/stores/getStores")
    Object getStores(@RequestParam("address") String postalCode,
                     @RequestParam int radius,
                     @RequestParam boolean pharmacyOnly,
                     @RequestParam boolean globalZipCodeRequired);
    // Get COVID Stores
    // https://www.riteaid.com/services/ext/v2/stores/getStores?address=19380&attrFilter=PREF-112&fetchMechanismVersion=2&radius=50

    @GetMapping("/services/ext/v2/stores/getStores")
    CovidStoresResponseDTO getCovidStores(@RequestParam("address") String postalCode,
                                          @RequestParam int radius,
                                          @RequestParam String attrFilter,
                                          @RequestParam int fetchMechanismVersion);
}
