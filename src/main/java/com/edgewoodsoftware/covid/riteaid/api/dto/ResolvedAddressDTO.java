package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResolvedAddressDTO {
    private String addressLine;
    private String adminDistrict;
    private Integer altitude;
    private String confidence;
    private String calculationMethod;
    private String countryRegion;
    private String displayName;
    private String district;
    private String formattedAddress;
    private GeocodeBestViewDTO geocodeBestView;
    private Double latitude;
    private String locality;
    private Double longitude;
    private String postalCode;
    private String postalTown;
}
