package com.edgewoodsoftware.covid.riteaid.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StoresWithAvailableSlots {
    private List<StoreModel> stores;
    private List<String> storesChecked;
    private boolean slotsAvailable;
}
