package com.edgewoodsoftware.covid.riteaid.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HolidayHourModel {
    private String holidayDate;
    private String storeHours;
    private String pharmacyHours;
}
