package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StoreDTO {
    private Integer storeNumber;
    private String address;
    private String city;
    private String state;
    private String zipcode;
    private String timeZone;
    private String fullZipCode;
    private String fullPhone;
    private String locationDescription;
    private String storeHoursMonday;
    private String storeHoursTuesday;
    private String storeHoursWednesday;
    private String storeHoursThursday;
    private String storeHoursFriday;
    private String storeHoursSaturday;
    private String storeHoursSunday;
    private String rxHrsMon;
    private String rxHrsTue;
    private String rxHrsWed;
    private String rxHrsThu;
    private String rxHrsFri;
    private String rxHrsSat;
    private String rxHrsSun;
    private String storeType;
    private Double latitude;
    private Double longitude;
    private String name;
    private Double milesFromCenter;
    private List<String> specialServiceKeys;
    private Object event;
    private List<HolidayHourDTO> holidayHours;
    private PickupDateAndTimesDTO pickupDateAndTimes;
}
