package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreNearAddress {
    private String address;
    private StoresWithAvailableSlots storesWithAvailableSlots;
}
