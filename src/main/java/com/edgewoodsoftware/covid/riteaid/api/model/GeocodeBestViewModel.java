package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeocodeBestViewModel {
    private ElementsModel northEastElements;
    private ElementsModel southWestElements;
}
