package com.edgewoodsoftware.covid.riteaid.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CovidStoresResponseModel {
    @JsonProperty("Data")
    private DataModel data;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("ErrCde")
    private Object errorCode;

    @JsonProperty("ErrMsg")
    private Object errorMessage;

    @JsonProperty("ErrMsgDtl")
    private Object errorMessageDetail;
}


