package com.edgewoodsoftware.covid.riteaid.api.controller;

import com.edgewoodsoftware.covid.riteaid.api.model.*;
import com.edgewoodsoftware.covid.riteaid.api.service.RiteAidService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/riteaid/covid-vaccine")
public class RiteAidVaccineController {
    private final static String SUCCESS = "SUCCESS";

    private final RiteAidService riteAidService;

    @Autowired
    public RiteAidVaccineController(RiteAidService riteAidService) {
        this.riteAidService = riteAidService;
    }

    @GetMapping("/stores")
    public ResponseEntity<CovidStoresResponseModel> getCovidStoresNearAddress(@RequestParam String address,
                                                                              @RequestParam int radius) {
        if (StringUtils.isBlank(address) || radius <= 0) {
            return ResponseEntity.badRequest().build();
        }

        String cleanAddr = StringUtils.trim(address);

        final CovidStoresResponseModel covidStores = riteAidService.getCovidStores(cleanAddr, radius);

        if (covidStores.getData() == null || CollectionUtils.isEmpty(covidStores.getData().getStores())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(covidStores);
    }

    @GetMapping("/store/{storeId}/slots")
    public ResponseEntity<Object> getStoreSlots(@PathVariable int storeId) {
        if (storeId <= 0) {
            return ResponseEntity.badRequest().build();
        }

        StoreSlotsResponseModel model = riteAidService.checkVaccineSlots(storeId);

        if (model == null) {
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
        }

        if (!SUCCESS.equals(model.getStatus())) {
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
        }

        return ResponseEntity.ok(model);
    }



    @GetMapping(value = "/stores/slots-available-by-address")
    public ResponseEntity<StoresWithAvailableSlots> findAvailableSlotsAtStoresNearAddress(@RequestParam String address,
                                                                                          @RequestParam int radius) {
        if (StringUtils.isBlank(address) || radius <= 0) {
            return ResponseEntity.badRequest().build();
        }

        StoresWithAvailableSlots stores = riteAidService.findAvailableSlotsAtStoresNearAddress(address, radius);

        if (stores == null) {
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
        }

        if (!stores.isSlotsAvailable()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(stores);
        }

        return ResponseEntity.ok(stores);
    }

    @GetMapping("/stores/slots-available")
    public ResponseEntity<StoresNearAddressesWithAvailableSlots> findAvailableSlotsAtStoresNearAddress() {
        StoresNearAddressesWithAvailableSlots stores = riteAidService.findAllStoresWithAvailableSlotsNearAddresses();

        if (stores == null || CollectionUtils.isEmpty(stores.getStoresChecked())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(stores);
    }

    @PostMapping("/stores/slots-available")
    public ResponseEntity<StoresNearAddressesWithAvailableSlots> findAvailableSlotsAtStoresNearAddress(
            @RequestBody Addresses addresses) {

        if (addresses == null || CollectionUtils.isEmpty(addresses.getAddresses())) {
            return ResponseEntity.badRequest().build();
        }

        StoresNearAddressesWithAvailableSlots stores = riteAidService.findAllStoresWithAvailableSlotsNearAddresses(addresses.getAddresses());

        if (stores == null || CollectionUtils.isEmpty(stores.getStoresWithAvailableSlots())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(stores);
    }

    @GetMapping("/address/slots-available")
    public ResponseEntity<StoresNearAddressWithSlotsAvailable> findAvailableSlotsNearAddresses() {
        StoresNearAddressWithSlotsAvailable stores = riteAidService.findAvailableSlotsNearAddresses();

        if (stores == null || CollectionUtils.isEmpty(stores.getStoresNearAddresses())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(stores);
    }

    @PostMapping("/address/slots-available")
    public ResponseEntity<StoresNearAddressWithSlotsAvailable> findAvailableSlotsNearAddresses(
            @RequestBody Addresses addresses) {

        if (addresses == null || CollectionUtils.isEmpty(addresses.getAddresses())) {
            return ResponseEntity.badRequest().build();
        }

        StoresNearAddressWithSlotsAvailable stores = riteAidService.findAvailableSlotsNearAddresses(addresses.getAddresses());

        if (stores == null || CollectionUtils.isEmpty(stores.getStoresNearAddresses())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(stores);
    }
}
