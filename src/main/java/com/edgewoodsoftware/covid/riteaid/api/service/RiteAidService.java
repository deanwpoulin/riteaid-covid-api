package com.edgewoodsoftware.covid.riteaid.api.service;

import com.edgewoodsoftware.covid.riteaid.api.client.RiteAidApiClient;
import com.edgewoodsoftware.covid.riteaid.api.config.RiteAidApiConfig;

import com.edgewoodsoftware.covid.riteaid.api.dto.CovidStoresResponseDTO;
import com.edgewoodsoftware.covid.riteaid.api.dto.StoreDTO;
import com.edgewoodsoftware.covid.riteaid.api.dto.StoreSlotsResponseDTO;
import com.edgewoodsoftware.covid.riteaid.api.model.*;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RiteAidService {
    private static final String SUCCESS = "SUCCESS";
    private static final Type STORE_MODEL_TYPE_TOKEN = new TypeToken<List<StoreModel>>() {}.getType();

    private final RiteAidApiClient riteAidApiClient;
    private final RiteAidApiConfig config;
    private final ModelMapper modelMapper;

    @Autowired
    public RiteAidService(RiteAidApiClient riteAidApiClient, RiteAidApiConfig config, ModelMapper modelMapper) {
        this.riteAidApiClient = riteAidApiClient;
        this.config = config;
        this.modelMapper = modelMapper;
    }

    private List<StoreDTO> getDistinctStoresNearAddresses(List<String> addresses) {
        Map<Integer, StoreDTO> distinctStores = new HashMap<>();

        for (String address : addresses) {
            CovidStoresResponseDTO dto = riteAidApiClient.getCovidStores(address, config.getRadius(),
                    config.getCovidStoresAttrFilter(), config.getCovidStoresFetchMechanismVersion());

            if (dto == null || !SUCCESS.equals(dto.getStatus()) ||
                    dto.getData() == null || CollectionUtils.isEmpty(dto.getData().getStores())) {
                continue;
            }

            List<StoreDTO> storesToCheck = dto.getData().getStores();

            for (StoreDTO store : storesToCheck) {
                if (distinctStores.containsKey(store.getStoreNumber())) {
                    continue;
                }

                distinctStores.put(store.getStoreNumber(), store);
            }
        }

        if (distinctStores.isEmpty()) {
            return new ArrayList<>();
        }

        return new ArrayList<>(distinctStores.values());
    }

    private List<StoreModel> getStoresWithAvailableSlots(List<StoreDTO> storesToCheck) {
        List<StoreDTO> stores = new ArrayList<>();

        try {
            for (StoreDTO store : storesToCheck) {
                if (config.getDelay() > 0) {
                    Thread.sleep(config.getDelay());
                }

                StoreSlotsResponseDTO response = riteAidApiClient.checkSlots(store.getStoreNumber());

                if (responseHasNoSlots(response)) {
                    continue;
                }

                if (response.getData().getSlots().entrySet().stream().noneMatch(Map.Entry::getValue)) {
                    continue;
                }

                stores.add(store);
            }
        } catch (InterruptedException e) {
            log.info("Thread interrupted", e);
        }

        if (CollectionUtils.isEmpty(stores)) {
            return new ArrayList<>();
        }

        return modelMapper.map(stores, STORE_MODEL_TYPE_TOKEN);
    }

    private String storeToString(StoreDTO store) {
        return String.format("Store #%s - %s - %s %s, %s %s", store.getStoreNumber(), store.getName(), store.getAddress(),
                store.getCity(), store.getState(), store.getZipcode());
    }

    private boolean responseHasNoSlots(StoreSlotsResponseDTO response) {
        return response == null || !SUCCESS.equals(response.getStatus()) ||
                response.getData() == null || CollectionUtils.isEmpty(response.getData().getSlots());
    }

    public StoreSlotsResponseModel checkVaccineSlots(int store) {
        StoreSlotsResponseDTO dto = riteAidApiClient.checkSlots(store);

        if (dto == null) {
            return null;
        }

        return modelMapper.map(dto, StoreSlotsResponseModel.class);
    }

    public CovidStoresResponseModel getCovidStores(String postalCode, int radius) {
        CovidStoresResponseDTO dto = riteAidApiClient.getCovidStores(postalCode, radius, config.getCovidStoresAttrFilter(),
                config.getCovidStoresFetchMechanismVersion());

        if (dto == null || !SUCCESS.equals(dto.getStatus())) {
            return null;
        }

        return modelMapper.map(dto, CovidStoresResponseModel.class);
    }

    public StoresNearAddressesWithAvailableSlots findAllStoresWithAvailableSlotsNearAddresses() {
        return findAllStoresWithAvailableSlotsNearAddresses(config.getAddresses());
    }

    public StoresNearAddressesWithAvailableSlots findAllStoresWithAvailableSlotsNearAddresses(List<String> addresses) {
        List<StoreDTO> storesToCheck = getDistinctStoresNearAddresses(addresses);

        if (CollectionUtils.isEmpty(storesToCheck)) {
            return null;
        }

        List<StoreModel> storesWithAvailability = getStoresWithAvailableSlots(storesToCheck);

        return StoresNearAddressesWithAvailableSlots.builder()
                .storesWithAvailableSlots(storesWithAvailability)
                .storesChecked(storesToCheck.stream().map(this::storeToString).collect(Collectors.toList()))
                .build();
    }

    public StoresNearAddressWithSlotsAvailable findAvailableSlotsNearAddresses(List<String> addresses) {
        StoresNearAddressWithSlotsAvailable storesNearAddressWithSlotsAvailable = new StoresNearAddressWithSlotsAvailable();

        List<StoreNearAddress> storeNearAddresses = new ArrayList<>();
        storesNearAddressWithSlotsAvailable.setStoresNearAddresses(storeNearAddresses);

        for (String address : addresses) {
            StoreNearAddress storeNearAddress = new StoreNearAddress();

            storeNearAddress.setAddress(address);
            StoresWithAvailableSlots storesWithAvailableSlots = findAvailableSlotsAtStoresNearAddress(address, config.getRadius());
            storeNearAddress.setStoresWithAvailableSlots(storesWithAvailableSlots);

            storeNearAddresses.add(storeNearAddress);
        }

        return storesNearAddressWithSlotsAvailable;
    }

    public StoresNearAddressWithSlotsAvailable findAvailableSlotsNearAddresses() {
        List<String> addresses = config.getAddresses();

        return findAvailableSlotsNearAddresses(addresses);
    }

    public StoresWithAvailableSlots findAvailableSlotsAtStoresNearAddress(String address, int radius) {
        CovidStoresResponseDTO dto = riteAidApiClient.getCovidStores(address, radius, config.getCovidStoresAttrFilter(),
                config.getCovidStoresFetchMechanismVersion());

        if (dto == null || !SUCCESS.equals(dto.getStatus()) ||
                dto.getData() == null || CollectionUtils.isEmpty(dto.getData().getStores())) {
            return null;
        }

        List<StoreDTO> storesToCheck = dto.getData().getStores();
        List<StoreModel> stores = getStoresWithAvailableSlots(storesToCheck);

        return StoresWithAvailableSlots.builder()
                .stores(stores)
                .storesChecked(storesToCheck.stream()
                        .map(this::storeToString)
                        .collect(Collectors.toList()))
                .slotsAvailable(!CollectionUtils.isEmpty(stores))
                .build();
    }
}
