package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StoresNearAddressWithSlotsAvailable {
    List<StoreNearAddress> storesNearAddresses;
}
