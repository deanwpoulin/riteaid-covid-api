package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreAndSlotsModel {
    private StoreModel store;
    private StoreSlotsDataModel slots;
}
