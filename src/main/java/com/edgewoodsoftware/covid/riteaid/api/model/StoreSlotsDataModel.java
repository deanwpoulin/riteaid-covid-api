package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreSlotsDataModel {
    private SlotsModel slots;
}
