package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class SlotsDTO extends HashMap<Integer, Boolean> { }
