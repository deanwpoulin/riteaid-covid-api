package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VaccineCheckSlotsResponseDTO {
    /*
    {"Data":{"slots":{"1":false,"2":false}},"Status":"SUCCESS","ErrCde":null,"ErrMsg":null,"ErrMsgDtl":null}
     */
}
