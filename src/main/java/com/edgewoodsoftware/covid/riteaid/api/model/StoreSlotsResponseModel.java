package com.edgewoodsoftware.covid.riteaid.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreSlotsResponseModel {
    @JsonProperty("Data")
    private StoreSlotsDataModel data;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("ErrCde")
    private String errorCode;

    @JsonProperty("ErrMsg")
    private String errorMessage;

    @JsonProperty("ErrMsgDtl")
    private String errorMessageDetail;
}

