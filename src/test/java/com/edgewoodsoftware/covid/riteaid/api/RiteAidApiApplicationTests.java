package com.edgewoodsoftware.covid.riteaid.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({ "unittest" })
class RiteAidApiApplicationTests {
	@Test
	void contextLoads() {
	}
}
