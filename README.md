# RiteAid Covid API

A simple web service that interacts with the RiteAid API given a list of configured cities or postal codes to search to find available appointment slots

## Requirements

To build/run this application you need the following installed:

- JDK 11

## Building The Service

To compile the applicaiton you use the Maven wrapper and execute:

```shell
./mvnw clean install
```

To run the spring boot app you use the Maven wrapper can execute:

```shell
./mvnw spring-boot:run
```

To build a docker image you can run:

```shell
./mvnw spring-boot:build-image
```

## Run The Service with Docker Compose

To run the currently published docker image (v0.0.1) you can run:

```shell
docker-compose up
```

A docker-compose.yml file exists in the root of this repository:

```yaml
services:
  riteaid-covid-api:
    image: registry.gitlab.com/edgewood-software/riteaid-covid-api/image:0.0.1
    restart: always
    ports:
      - "8080:8080"
    environment:
      SPRING_PROFILES_ACTIVE: docker
```

## REST Endpoints

This Spring Boot app exposes the following endpoints:

### Get Covid Vaccine Stores Near an Address

GET /riteaid/covid-vaccine/stores?address=address={address|ZIP|City, State}&radius={int}

### Get Slots Available at a Specific Store

GET /riteaid/covid-vaccine/store/{storeId}/slots

### Get Stores With Slots Available Near an Address

GET /riteaid/covid-vaccine/stores/slots-available?address={address|ZIP|City, State}&radius={int}

### Get Stores with Slots Available Using Configured Addresses

This will use the `rite-aid.api.addresses` parameter in application.yml as the source of the addresses to find stores.

GET /riteaid/covid-vaccine/stores/slots-available

### Get Stores with Slots Available Passing in A List of Specific Addresses

This will use the addresses provided to search for stores and check for available appointments

POST /riteaid/covid-vaccine/stores/slots-available

Request Body

```json
{
  "addresses": [
    "19406",
    "Phoenixville, PA",
    "123 Main St Phoenixville, PA",
    "Erie, PA"
  ]
}
```

### Find Slots Available Grouped by the Address Searched

GET /riteaid/covid-vaccine/address/slots-available

### Find Slots Available Grouped by the Address Searched

POST /riteaid/covid-vaccine/address/slots-available

Request Body

```json
{
  "addresses": [
    "19406",
    "Phoenixville, PA",
    "123 Main St Phoenixville, PA",
    "Erie, PA"
  ]
}
```

## Swagger Documentation

You can view swagger documentation by running this spring boot service and visiting the URL:

http://localhost:8080/swagger-ui/
